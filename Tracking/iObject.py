"""
Object to track. Class is necessary for tracking multiple peoples.
"""

from LabelObj import LabelObj

label_conf_thresh = 10

def overlap(x1, w1, x2, w2):
    l1 = x1 - w1/2
    l2 = x2 - w2/2
    left = l1 if (l1 > l2) else l2
    r1 = x1 + w1/2
    r2 = x2 + w2/2
    right = r1 if (r1 < r2) else r2
    #print("left : %f, right : %f" % (left, right))
    return right - left

def iou(box1, box2): #box = (topleft, bottomright)
    w1 = box1[1][0] - box1[0][0]
    h1 = box1[1][1] - box1[0][1]
    w2 = box2[1][0] - box2[0][0]
    h2 = box2[1][1] - box2[0][1]
    x1 = box1[1][0] + w1/2
    y1 = box1[1][1] + h1/2
    x2 = box2[1][0] + w2/2
    y2 = box2[1][1] + h2/2
    #print("x1 : %f, w1 : %f, x2 : %f, w2 : %f" % (x1, w1, x2, w2))
    w = overlap(x1, w1, x2, w2)
    h = overlap(y1, h1, y2, h2)
    #print("w : %f" % w)
    #print("h : %f" % h)
    if (w < 0 or h < 0):
        return 0
    area = w * h
    union = w1 * h1 + w2 * h2 - area
    #print("area : %f" % area)
    #print("union : %f" % union)
    return float(area) / union

class iObject:

    def __init__(self, filename, totalframes, objectID, size):
        self.cache = ((-1, -1), (-1, -1))
        self.datacounter = 0
        self.dict = {}
        self.filename = filename
        self.objectID = objectID
        self.size = size
        self.totalframes = totalframes
        self.initDict()
        self.allData = {}

    def obj2box(self, obj):
        imwidth, imheight = self.size
        rect_pos = [imwidth * (float(obj[1]) - 0.5 * float(obj[3])), imheight * (float(obj[2]) - 0.5 * float(obj[4])), imwidth * float(obj[3]), imheight * float(obj[4])]
        tl = (int(rect_pos[0]), int(rect_pos[1]))
        br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
        return (tl, br)

    def initAllData(self, startframe=0):
        original_dict = self.dict
        original_objectID = self.objectID
        for i in range (0, 80): # 80 coco classes
            self.objectID = i
            try:
                self.initDict(startframe)
            except IndexError:
                continue
            self.allData[i] = self.dict
        self.dict = original_dict
        self.objectID = original_objectID

    def initDict(self, startframe=0):
        objectID = self.objectID
        imwidth, imheight = self.size
        #print(self.totalframes)
        for framenum in range(startframe, self.totalframes - 10):
            label = LabelObj(self.filename, framenum)
            if (label.getObjConfidence(objectID) > label_conf_thresh):
                objdata_list = []
                objdata = label.getObjData(objectID)
                while (objdata[0] != "-1"):
                    objdata_list.append(objdata)
                    objdata = label.getNextObjData(objectID)
                maxiou_data = objdata_list[0]
                maxiou = 0.001
                for obj in objdata_list:
                    box = self.obj2box(obj)
                    tempiou = iou(self.cache, box)
                    if (tempiou > maxiou):
                        maxiou = tempiou
                        maxiou_data = obj
                #print("\n\nIndex:")
                #print(objdata_list.index(maxiou_data))
                self.dict[framenum] = self.obj2box(maxiou_data)
                self.cache = self.dict[framenum]

    def resetFromCache(self, framenum, cache):
        self.cache = cache
        self.initDict(framenum)

    def switchObj(self, framenum):
        label = LabelObj(self.filename, framenum)
        box_list = []
        objdata = label.getObjData(self.objectID)
        while (objdata[0] != "-1"):
            box_list.append(self.obj2box(objdata))
            objdata = label.getNextObjData(self.objectID)
        index = box_list.index(self.dict[framenum]) + self.datacounter
        while (index < 0):
            index += len(box_list)
        while (index > len(box_list) - 1):
            index -= len(box_list)
        #print(index)
        self.resetFromCache(framenum, box_list[index])

    def nextObj(self, framenum):
        self.datacounter += 1
        self.switchObj(framenum)

    def prevObj(self, framenum):
        self.datacounter -= 1
        self.switchObj(framenum)
