"""
LabelObj is an object that represents the a yolo format label.
"""

class LabelObj:

    def __init__(self, videofilename, framenum):
        self.videofilename = videofilename
        self.framenum = framenum
        self.labelname = "%s_%d.txt" % (videofilename, framenum)
        self.confname = "%s_%d_confidence.txt" % (videofilename, framenum)
        self.readToArray()
        self.objCounter = 0

    def readToArray(self):
        readIn = []
        confIn = []
        #print("labels/%s" % self.labelname)
        text_file = open("labels/%s" % self.labelname, "r")
        for line in text_file:
            readIn.append(line.strip('\n').split(' '))
        try:
            conf_file = open("labels/%s" % self.confname, "r")
            for line in conf_file:
                confIn.append(line.strip('\n').split(' '))
        except:
            print("Warning : conf_file not found : %s\n" % conf_file)
        self.data = readIn
        self.conf = confIn
        return

    def getObjData(self, obj): #return first obj that matches
        for set in self.data:
            try:
                if(int(set[0]) == obj):
                    return set
            except:
                continue
        return ["-1"]

    def getNextObjData(self, obj):
        self.objCounter += 1
        objCounter = self.objCounter
        for set in self.data:
            try:
                if(int(set[0]) == obj):
                    if (objCounter == 0):
                        return set
                    else:
                        objCounter -= 1
            except:
                continue
        return ["-1"]


    def getObjConfidence(self, obj):
        for set in self.conf:
            try:
                if(int(set[0]) == obj):
                    return int(set[1])
            except:
                continue
        return -1
