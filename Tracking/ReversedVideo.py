"""
ReversedVideo takes care of reverse object tracking.
"""
import os
import subprocess
import cv2
from LabelObj import *

from time import time

class ReversedVideo:

    def __init__(self, videofilename, bacf):
        self.videofilename = videofilename
        self.bacf = bacf
        self.reversed = cv2.VideoCapture(self.reverseVideo(videofilename))
        self.dict = self.track()

    def reverseVideo(self, videofilename):
        print("\nProcessing %s in reverse ..." % videofilename)
        subprocess.check_output(['rm', 'reversed.mp4'])
        subprocess.check_output(['bash', 'reverse.sh', videofilename, 'reversed.mp4'])
        print("\nReversal complete! Initializing tracker ...")
        return "reversed.mp4"

    def track(self):
        bacf = self.bacf
        objectID = 0
        trackingOn = False
        filename = self.videofilename
        cap = self.reversed
        imwidth = cap.get(3)
        imheight = cap.get(4)
        framenum = -1
        totalframes = int(cap.get(7))

        #print("\n\nValues : ")
        #print(totalframes)
        #print(framenum)

        dict = {}

        print("\nInitializing backwards tracker ...\n\n")
        while(framenum < totalframes):
            framenum = int(cap.get(1))
            ret, frame = cap.read()
            if not ret:
                break
            if (framenum < 10): #cut off ending
                continue
            if (framenum % (totalframes/10) == 0):
                print("\n%d%% complete...." % (round(float(framenum) / totalframes * 100)))
            #print(framenum)
            label = LabelObj(filename, totalframes - framenum)
            #print(framenum)
            if(label.getObjConfidence(objectID) > 95): #reset obj tracker when labels are highly accurate
                objdata = label.getObjData(objectID)
                if (objdata[0] == "-1"):
                    trackingOn = False
                    continue
                rect_pos = [imwidth * (float(objdata[1]) - 0.5 * float(objdata[3])), imheight * (float(objdata[2]) - 0.5 * float(objdata[4])), imwidth * float(objdata[3]), imheight * float(objdata[4])]
                tracker = bacf.init(frame, rect_pos)
                trackingOn = True
                tracker, response = bacf.track(frame)
                bacf.train(frame)
                _, rect_pos, _, _ = bacf.get_state()
                tl = (int(rect_pos[0]), int(rect_pos[1]))
                br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
                dict[(totalframes - framenum)] = [bacf._get_psr(response), tl, br]
            elif(trackingOn):
                tracker, response = bacf.track(frame)
                bacf.train(frame)
                _, rect_pos, _, _ = bacf.get_state()
                #print(bacf._get_psr(response))
                if (bacf._get_psr(response) < 3.8): #set threshold for tracking confidence
                    continue
                tl = (int(rect_pos[0]), int(rect_pos[1]))
                br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
                dict[(totalframes - framenum)] = [bacf._get_psr(response), tl, br]

        cap.release()
        print("\nReverse tracking completed!")
        cv2.destroyAllWindows()
        return dict
