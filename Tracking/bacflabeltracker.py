import numpy as np
import cv2
import sys
from time import time

import traceback
import matplotlib as mpl
mpl.use("tkagg")

from background_aware_correlation_filter import BackgroundAwareCorrelationFilter as BACF
from utils.arg_parse import parse_args
from image_process.feature import get_pyhog
from utils.get_sequence import get_sequence_info, load_image
from utils.report import LogManger

from LabelObj import *
from ReversedVideo import *

selectingObject = False
initTracking = False
onTracking = False
ix, iy, cx, cy = -1, -1, -1, -1
w, h = 0, 0

interval = 1
duration = 0.01

objectID = 0
trackingOn = False

reverseTrackingToggle = False

cached_area = -1.0

def area(tl, br):
    return ((br[0] - tl[0]) * (br[1] - tl[1]))

def num_near(n1, n2):
    if (n1 > n2):
        if (n2 / n1 > 0.5):
            return True
    else:
        if (n1 / n2 > 0.5):
            return True

def overlap(x1, w1, x2, w2):
    l1 = x1 - w1/2
    l2 = x2 - w2/2
    left = l1 if (l1 > l2) else l2
    r1 = x1 + w1/2
    r2 = x2 + w2/2
    right = r1 if (r1 < r2) else r2
    #print("left : %f, right : %f" % (left, right))
    return right - left

def iou(box1, box2): #box = (topleft, bottomright)
    w1 = box1[1][0] - box1[0][0]
    h1 = box1[1][1] - box1[0][1]
    w2 = box2[1][0] - box2[0][0]
    h2 = box2[1][1] - box2[0][1]
    x1 = box1[1][0] + w1/2
    y1 = box1[1][1] + h1/2
    x2 = box2[1][0] + w2/2
    y2 = box2[1][1] + h2/2
    #print("x1 : %f, w1 : %f, x2 : %f, w2 : %f" % (x1, w1, x2, w2))
    w = overlap(x1, w1, x2, w2)
    h = overlap(y1, h1, y2, h2)
    #print("w : %f" % w)
    #print("h : %f" % h)
    if (w < 0 or h < 0):
        return 0
    area = w * h
    union = w1 * h1 + w2 * h2 - area
    #print("area : %f" % area)
    #print("union : %f" % union)
    return float(area) / union

if __name__ == '__main__':
    filename = "royceanderic4.mp4"
    cap = cv2.VideoCapture(filename)
    interval = 30

    #parser = parse_args()
    #params = parser.parse_args()
    """for param in dir(params):
        if not param.startswith('_'):
            print('{0} : {1}'.format(param, getattr(params, param)))"""


    bacf = BACF(get_pyhog, admm_lambda=0.01,
                cell_selection_thresh=0.5625,
                dim_feature=31,
                filter_max_area=2500,
                feature_ratio=4,
                interpolate_response=4,
                learning_rate=0.013,
                search_area_scale=5.0,
                reg_window_power=2,
                n_scales=5,
                newton_iterations=5,
                output_sigma_factor=0.0625,
                refinement_iterations=1,
                reg_lambda=0.01,
                reg_window_edge=3.0,
                reg_window_min=0.1,
                scale_step=1.01,
                search_area_shape='square',
                save_without_showing=False,
                debug=False,
                visualization=True,
                is_redetection=False,
                redetection_search_area_scale=2.0,
                is_entire_redection=False,
                psr_threshold=2.0)

    log_manager = LogManger(bacf, './result', ["rect_pos", "psr"],
                            elapsed_time=False,
                            visualization=True,
                            is_detailed=False,
                            is_simplest=False,
                            save_without_showing=False)
    if reverseTrackingToggle:
        reverseDict = ReversedVideo(filename, bacf).dict

    imwidth = cap.get(3)
    imheight = cap.get(4)

    #cv2.namedWindow('preview')

    framenum = -1
    totalframes = int(cap.get(7))
    begin = False
    while(framenum < totalframes - 2):

        framenum = int(cap.get(1))
        label = LabelObj(filename, framenum)
        #print(framenum)
        ret, frame = cap.read()
        if not ret:
            break

        if(label.getObjConfidence(objectID) > 95): #reset obj tracker when labels are highly accurate

            objdata_list = []
            objdata = label.getObjData(objectID)
            while (objdata[0] != "-1"):
                objdata_list.append(objdata)
                objdata = label.getNextObjData(objectID)
            maxiou_data = objdata_list[0]
            maxiou = 0.0

            for obj in objdata_list:
                rect_pos = [imwidth * (float(obj[1]) - 0.5 * float(obj[3])), imheight * (float(obj[2]) - 0.5 * float(obj[4])), imwidth * float(obj[3]), imheight * float(obj[4])]
                tl = (int(rect_pos[0]), int(rect_pos[1]))
                br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
                try:
                    tempiou = iou((cached_tl, cached_br), (tl, br))
                    if (tempiou > maxiou):
                        maxiou = tempiou
                        maxiou_data = obj
                except:
                    break

            objdata = maxiou_data

            """datacounter = 0

            key = cv2.waitKey(100) & 0xff
            if key == ord(' '):
                while True:

                    key = cv2.waitKey(1) or 0xff
                    cv2.imshow('preview', frame)

                    if key == ord(' '):
                        break
                    elif key == ord('a'):
                        datacounter -= 1
                    elif key == ord('d'):
                        datacounter += 1
                    while (datacounter >= len(objdata_list)):
                        datacounter -= len(objdata_list)
                    while (datacounter < 0):
                        datacounter += len(objdata_list)
                    objdata = objdata_list[datacounter]"""

            """key = cv2.waitKey(1) or 0xff
            #cv2.imshow('preview', frame)

            if key == ord('a'):
                datacounter -= 1
            elif key == ord('d'):
                datacounter += 1
            while (datacounter >= len(objdata_list)):
                datacounter -= len(objdata_list)
            while (datacounter < 0):
                datacounter += len(objdata_list)
            objdata = objdata_list[datacounter]"""


            #print(objdata)

            rect_pos = [imwidth * (float(objdata[1]) - 0.5 * float(objdata[3])), imheight * (float(objdata[2]) - 0.5 * float(objdata[4])), imwidth * float(objdata[3]), imheight * float(objdata[4])]
            tl = (int(rect_pos[0]), int(rect_pos[1]))
            br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))

            """while(not matched):
                if (objdata[0] == "-1"):
                    trackingOn = False
                    break

                rect_pos = [imwidth * (float(objdata[1]) - 0.5 * float(objdata[3])), imheight * (float(objdata[2]) - 0.5 * float(objdata[4])), imwidth * float(objdata[3]), imheight * float(objdata[4])]
                tl = (int(rect_pos[0]), int(rect_pos[1]))
                br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))

                if (framenum < 5):
                    break

                #print("Pairs: ")
                #print((cached_tl, cached_br))
                #print((tl, br))

                #print(iou((cached_tl, cached_br), (tl, br)))

                #if ((cached_area == -1.0 or (num_near(cached_area, area(tl, br)))) and iou((cached_tl, cached_br), (tl, br)) > 0.5):
                if (iou((cached_tl, cached_br), (tl, br)) > 0.5):
                    matched = True
                else:
                    objdata = label.getNextObjData(objectID)
                    print(objdata)"""

            tracker = bacf.init(frame, rect_pos)
            trackingOn = True

            log_manager.init(['Demo'], "BACF")
            log_manager.store(**{"rect_pos": rect_pos})

            t0 = time()
            tracker, response = bacf.track(frame)
            t1 = time()

            bacf.train(frame)

            _, rect_pos, _, _ = bacf.get_state()

            tl = (int(rect_pos[0]), int(rect_pos[1]))
            br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
            cv2.rectangle(frame, tl, br, (255, 0, 0), 3)

            duration = 0.8*duration + 0.2*(t1-t0)
            cv2.putText(frame, 'FPS: '+str(1/duration)[:4].strip('.'), (8,20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0,0,255), 2)
            log_manager.visualize()

            if not begin:
                #print("hi")
                #origin_feature = bacf._extract_feature(frame, bacf._position, bacf._scale_factor)
                #origin_data = bacf.roycedata
                #origin_bacf = bacf
                begin = True
            #else:
                #print("\n\nCorrelation:")
                #print(bacf._correlate_with_filter(origin_feature))
                #print("\n\nDisplacement:")
                #print(bacf._find_displacement(origin_data[0], origin_data[1], frame, origin_data[3], bacf._get_psr(response)))
                #print("\n\nPSR:")
                #print(bacf._get_psr(origin_data[0]))
                #temp_bacf = origin_bacf
                #tracker, response = temp_bacf.track(frame)
                #print("\n\nPSR:")
                #print(bacf._get_psr(response))
                #print(response)
                #print("Features:")
                #print((bacf.features.shape))
                #print(abs(np.sum(abs(np.subtract(bacf._extract_feature(frame, bacf._position, bacf._scale_factor), origin_feature)))))
                #print(np.sum(np.subtract(origin_bacf.features, bacf.features)))

        elif(trackingOn):
            t0 = time()
            tracker, response = bacf.track(frame)
            t1 = time()

            bacf.train(frame)

            _, rect_pos, _, _ = bacf.get_state()

            #print(bacf._get_psr(response))

            tl = (int(rect_pos[0]), int(rect_pos[1]))
            br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))

            conf = bacf._get_psr(response)

            try:
                if (reverseTrackingToggle and reverseDict[framenum][0] > conf): #if reverse tracking gave greater confidence
                    tl = reverseDict[framenum][1]
                    br = reverseDict[framenum][2]
                elif (conf < 3.8): #set threshold for tracking confidence
                    continue
            except KeyError:
                if (conf < 3.8):
                    continue
                else:
                    pass

            cv2.rectangle(frame, tl, br, (255, 0, 0), 3)

            duration = 0.8*duration + 0.2*(t1-t0)
            cv2.putText(frame, 'FPS: '+str(1/duration)[:4].strip('.') + '  [Tracking...]', (8,20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0,0,255), 2)

            log_manager.visualize()

        else:
            """objdata = label.getObjData(objectID)
            objx1 = float(objdata[1]) - float(objdata[3] / 2.0)
            objx2 = float(objdata[1]) + float(objdata[3] / 2.0)
            objy1 = float(objdata[2]) - float(objdata[4] / 2.0)
            objy2 = float(objdata[2]) + float(objdata[4] / 2.0)
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0,255,255), 1)
            cv2.putText(frame, "Using Groundtruth Data ...", (8,20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0,0,255), 2)"""
            cv2.putText(frame, "Warning : Object of ID [%d] could not be detected ..." % objectID, (8,20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0,0,255), 2)

        cv2.imshow('preview', frame)

        #cached_rect_pos = rect_pos
        #print(cached_rect_pos)

        #print("\n")
        #print(cached_area)
        #print(area(tl, br))
        if begin:
            cached_tl = tl
            cached_br = br
            cached_area = area(tl, br)
        #print("cached.")

        c = cv2.waitKey(interval) & 0xFF
        if c==27 or c==ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
