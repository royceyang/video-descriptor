import numpy as np
import cv2
import sys
import time

"""import traceback
import matplotlib as mpl
mpl.use("tkagg")"""

from background_aware_correlation_filter import BackgroundAwareCorrelationFilter as BACF
from utils.arg_parse import parse_args
from image_process.feature import get_pyhog
from utils.get_sequence import get_sequence_info, load_image
from utils.report import LogManger

from LabelObj import *
from ReversedVideo import *
from iObject import *

import pickle
import os
import subprocess
import argparse

interval = 1
reverseTracking = False
mixedObjects = False

selectingObject = False
initTracking = False
onTracking = False
pause = False
ix, iy, cx, cy = -1, -1, -1, -1
w, h = 0, 0

objectID = 0 # set object that you want to track.. 0 is people

framenum = -1

objnum = 0
obj_list = []

export = False #save images and labels?

def mouseclick(event, x, y, flags, param):
    global selectingObject, initTracking, ix, iy, cx,cy, w, h, pause, obj_list, objnum, singleTracking
    if event == cv2.EVENT_MBUTTONDOWN:
        objnum += 1
        if (objnum >= len(obj_list)):
            objnum -= len(obj_list)
        #print("\nCurrent ID: %d/%d" % (objnum + 1, len(obj_list)))
    elif event == cv2.EVENT_LBUTTONDOWN:
        selectingObject = True
        ix, iy = x, y
        cx, cy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
		cx, cy = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        selectingObject = False
        pause = False
        if mixedObjects:
            newID = -1
            maxiou = 0.001
            maxiou_loc = None
            obj_list[objnum].cache = ((ix, iy), (x, y))
            #print(obj1.allData[0][framenum])
            for i in (0, 80):
                try:
                    temp_loc = obj_list[objnum].allData[i][framenum]
                    iou_val = iou(obj_list[objnum].cache, temp_loc)
                    if (iou_val > maxiou):
                        newID = i
                        maxiou = iou_val
                        maxiou_loc = temp_loc
                except:
                    continue
            obj_list[objnum].objectID = newID
            obj_list[objnum].resetFromCache(framenum, maxiou_loc)
        elif (abs(x-ix)>10 and abs(y-iy)>10):
            initTracking = True
            obj_list[objnum].resetFromCache(framenum, ((ix, iy), (x, y)))
    """elif event == cv2.EVENT_RBUTTONDOWN:
        if not pause:
            pause = True
        else:
            pause = False
        print(pause)"""

def initBACF():
    return BACF(get_pyhog, admm_lambda=0.01,
                cell_selection_thresh=0.5625,
                dim_feature=31,
                filter_max_area=2500,
                feature_ratio=4,
                interpolate_response=4,
                learning_rate=0.013,
                search_area_scale=5.0,
                reg_window_power=2,
                n_scales=5,
                newton_iterations=5,
                output_sigma_factor=0.0625,
                refinement_iterations=1,
                reg_lambda=0.01,
                reg_window_edge=3.0,
                reg_window_min=0.1,
                scale_step=1.01,
                search_area_shape='square',
                save_without_showing=False,
                debug=False,
                visualization=True,
                is_redetection=False,
                redetection_search_area_scale=2.0,
                is_entire_redection=False,
                psr_threshold=2.0)

def initLog(bacf):
    return LogManger(bacf, './result', ["rect_pos", "psr"],
                            elapsed_time=False,
                            visualization=True,
                            is_detailed=False,
                            is_simplest=False,
                            save_without_showing=False)

def corners2RectPos(tl, br):
    return [float(tl[0]), float(tl[1]), float(br[0] - tl[0]), float(br[1] - tl[1])]

def resetTrackingFrom0():
    global interval, reverseTracking, mixedObjects, selectingObject, initTracking
    global onTracking, pause, ix, iy, cx, cy, w, h, objectID, framenum
    global objnum, obj_list
    #interval = 30
    #reverseTracking = False
    #mixedObjects = False
    selectingObject = False
    initTracking = False
    onTracking = False
    pause = False
    ix, iy, cx, cy = -1, -1, -1, -1
    w, h = 0, 0
    objectID = 0
    framenum = -1
    objnum = 0
    obj_list = []

def setFrameNum(num):
    global framenum
    framenum = num - 1


def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def multiTrack(filename, numtrackers):
    global obj_list, objnum, framenum, objectID, reverseTracking, mixedObjects
    global selectingObject, initTracking, ix, iy, cx,cy, w, h, pause
    cap = cv2.VideoCapture(filename)
    totalframes = int(cap.get(7))
    bacf = initBACF()
    log_manager = initLog(bacf)
    colors = [(0, 255, 0), (127, 0, 255), (255, 128, 0), (0, 255, 255), (255, 0, 0)]
    if reverseTracking:
        reverseDict = ReversedVideo(filename, bacf).dict
    #TODO: init trackers from labels

    totalframes = int(cap.get(7))

    imsize = (cap.get(3), cap.get(4))

    cv2.namedWindow('Preview %s' % filename)
    cv2.setMouseCallback('Preview %s' % filename, mouseclick)
    cv2.createTrackbar("Frames", 'Preview %s' % filename, 0, totalframes, setFrameNum)

    if not os.path.exists("%s_save.p" % filename):
        for i in range(0, numtrackers):
            obj_list.append(iObject(filename, totalframes, objectID, imsize))
            for j in range(0, i):
                obj_list[i].nextObj(0)
    else:
        obj_list = pickle.load(open("%s_save.p" % filename, "rb"))

    #TODO:if mixedObjects:
        #obj1.initAllData()

    tracker = None
    initialized = False

    while (framenum < totalframes - 11): #11 is arbitrary number to ensure index in bounds
        framenum += 1
        cap.set(1, framenum)
        ret, frame = cap.read()
        if not ret:
            break
        cv2.setTrackbarPos("Frames", 'Preview %s' % filename, framenum)

        if export:
            cv2.imwrite("%s_data/%s_%d.jpg" % (filename, filename, framenum), frame)
            labelfile = open("%s_data/%s_%d.txt" % (filename, filename, framenum), "w")

        for obj1 in obj_list:
            if framenum in obj1.dict:
                initialized = True
                tl, br = obj1.dict[framenum]
                rect_pos = corners2RectPos(tl, br)
                tracker = bacf.init(frame, rect_pos)
                cv2.rectangle(frame, tl, br, colors[obj_list.index(obj1)], 3)
                log_manager.init(['Demo'], "BACF")
                log_manager.store(**{"rect_pos": rect_pos})
            elif initialized and tracker is not None:
                tracker, response = bacf.track(frame)
                bacf.train(frame)
                _, rect_pos, _, _ = bacf.get_state()
                tl = (int(rect_pos[0]), int(rect_pos[1]))
                br = (int(rect_pos[0] + rect_pos[2]), int(rect_pos[1] + rect_pos[3]))
                cv2.rectangle(frame, tl, br, colors[obj_list.index(obj1)], 3)

            if selectingObject:
                cv2.rectangle(frame, (ix, iy), (cx, cy), (0, 255, 255), 1)
            elif export:
                b = (float(tl[0]), float(br[0]), float(tl[1]), float(br[1]))
                formatted_txt = convert(imsize, b)
                labelfile.write("%d %f %f %f %f" % (objectID, formatted_txt[0], formatted_txt[1], formatted_txt[2], formatted_txt[3]))
                if (obj_list.index(obj1) != len(obj_list) - 1):
                    labelfile.write("\n")
        if export:
            labelfile.close()

        cv2.putText(frame, 'Selected Object ID : %d/%d' % (objnum+1, len(obj_list)), (8,20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, colors[objnum], 2)

        cv2.imshow('Preview %s' % filename, frame)
        if initialized:
            log_manager.visualize()

def setUpExport(filename):
    if not os.path.exists('%s_data' % filename):
        os.makedirs('%s_data' % filename)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="video file that you want to process")
    parser.add_argument("-c", "--clean", action="store_true", help="remove saved data from previous sessions")
    parser.add_argument("-x", "--export", action="store_true", help="export your labels, to be used for training")
    parser.add_argument("--classfile", help="path to classes.names, if you want the data to be packaged for training")
    parser.add_argument("-n", "--numobjects", help="number of objects to track", type=int)
    parser.add_argument("-id", "--objectID", help="index of object you want to track in classfile", type=int)
    args = parser.parse_args()
    filename = args.filename
    if args.objectID is not None:
        objectID = args.objectID
    if args.numobjects is not None:
        numobjects = args.numobjects
    else:
        numobjects = 2
    if args.clean:
        try:
            subprocess.check_output(['sudo', 'rm', '%s_save.p' % filename])
        except:
            print("\nWarning : Nothing to clean!\n")
            pass
    if args.export:
        export = True
        print("\nSetting up export folder : %s_data ..." % filename)
        setUpExport(filename)
        print("Done!")
    if args.export and args.classfile is not None:
        subprocess.check_output(['cp', args.classfile, '%s_data/classes.names' % filename])
    try:
        multiTrack(filename, numobjects)
        if args.export and args.classfile is not None:
            subprocess.check_output(['python', 'Preprocessing.py', '%s_data' % filename])
    except KeyboardInterrupt:
        pass

    print("\n\nSaving session data ...")
    pickle.dump(obj_list, open("%s_save.p" % filename, "wb"))
    print("Session data saved!\n")
