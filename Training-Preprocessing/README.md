# Training-Preprocessing

Training-Preprocessing is a computer vision preprocessing program that automatically coverts .txt format labels to .xml and vice versa, such that a complete dataset with images, xml annotations, and txt labels are prepared. Compared to other tools, this tools has very minimal set-up requirements and prepares files to the immediate standards of model training. The output is packaged together in a folder called Training-Data and very little extra effort is necessary to immediately begin the training process.

### Installation and Running the Demo

Training-Preprocessing is easy to install and run (the demo):

```sh
$ git clone https://gitlab.com/royceyang/Training-Preprocessing.git
$ cd Training-Preprocessing
$ python Preprocessing.py sample_data
```

Results are saved in the Training-Data directory.

Training-Preprocessing only has one dependency:

 - Pillow.

To install:

```sh
$ pip install Pillow
```

### Customization

If you would like to use your own data, there is only one change you need to make:
 - Edit classes.names for your customized classes.

### Running the Program

Put your classes.names, images, labels (.txt), annotations (.xml) all together in one directory. Note that there cannot be other subdirectories inside this directory.

To run:

```sh
$ python Preprocessing.py [name of folder with all the files together]
```


As of September 11, 2018, a custom feature has been added for when all labels are in one file:

```
$ python Preprocessing.py [name of folder with image files and classes.txt] [name of appended labels file]
```

Samples to reference for the new feature:
 - [name of folder with image files and classes.txt] = custom_images
 - [name of appended labels file] = custom_labels.txt

Demo for new feature:
```
$ python Preprocessing.py custom_images custom_labels.txt
```
