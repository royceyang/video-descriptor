#!/usr/bin/env python

"""
CompareClassification.py

Program for comparing the accuracy of video classifications. The AC class is
mechanically-labeled and PC is auto-labeled.
"""

__author__ = "Royce Yang"
__version__ = "1.1.3"

import os
from Classes.PredictedClassification import *
from Classes.ActualClassification import *

def main():
    confusion_total = [[0]*4 for i in range(4)]
    skip_total = [0, 0, 0]
    delay_total = [[0, 0, 0], [0, 0, 0]]
    correct = 0; incorrect = 0; counter = 0; skipcounter = 0; entrydelaycounter = 0; exitdelaycounter = 0;
    for filename in sorted(os.listdir("MechanicalLabels")):
        acfile = AC("MechanicalLabels/%s" % filename)

        try:
            pcfile = PC("AutoLabels/FuVersion/%s.txt" % acfile.getVideoName())
            #pcfile = PC("../test_results/%s.txt" % acfile.getVideoName())
        except FileNotFoundError:
            print("The AC label for %s cannot be found. Skipping ..." % acfile.getVideoName())

        acdata = acfile.data
        pcdata = pcfile.data
        acdata = fillArray(acdata, len(pcdata))
        acfile.setData(acdata)

        if (len(acdata) != len(pcdata)):
            print("\nWarning : Matrices length of %s not equal! (Check your labels!)\n" % acfile.getVideoName())

        print("\n\n\n********** Calculating results for %s **********" % acfile.getVideoName())
        matrix, temp_correct, temp_incorrect = confusion_matrix(acfile, pcfile)
        correct += temp_correct
        incorrect += temp_incorrect
        skip_m = skip_matrix(acfile, pcfile)
        skipcounter += skip_m[0] + skip_m[1] + skip_m[2]
        delay_m = delay_matrix(acfile, pcfile)
        entrydelaycounter += delay_m[0][0] + delay_m[0][1] + delay_m[0][2]
        exitdelaycounter += delay_m[1][0] + delay_m[1][1] + delay_m[1][2]

        print("\nConfusion Matrix : ")
        print2DMatrix(matrix)
        confusion_total = addArray(confusion_total, matrix)
        print("\nSkip Matrix : ")
        print(skip_m)
        skip_total = addArray(skip_total, skip_m)
        print("\nDelay Matrix : ")
        print2DMatrix(delay_m)
        delay_total = addArray(delay_total, delay_m)
        counter += 1

    print("\n\n\n***-------------- Aggregate Data --------------***")
    print("\nAverage Accuracy (across all %d files) : %.3f" % (counter, correct / (correct + incorrect)))
    print("Average Jump Error (across all %d files) : %.3f" % (counter, skipcounter / counter))
    print("Average Entry Delay Error (across all %d files) : %.3f" % (counter, entrydelaycounter / counter))
    print("Average Exit Delay Error (across all %d files) : %.3f" % (counter, exitdelaycounter / counter))
    print("")
    print("\nConfusion Matrix : ")
    print2DMatrix(confusion_total)
    print("\nSkip Matrix : ")
    print(skip_total)
    print("\nDelay Matrix : ")
    print2DMatrix(delay_total)

def arrColLen(arr):
    try:
        return len(arr[0])
    except TypeError:
        return 1

def addArray(arr1, arr2):
    result = arr1
    for i in range (len(arr1)):
        for j in range (arrColLen(arr1)):
            if (arrColLen(arr1) == 1):
                result[i] = arr1[i] + arr2[i]
            else:
                result[i][j] = arr1[i][j] + arr2[i][j]
    return result

def fillArray(ac, totalframes): #to fill the unlabeled parts of the AC array with "Nothing"
    for i in range (len(ac), totalframes):
        ac.append([i + 1, -1])
    return ac

def print2DMatrix(m):
    for i in range (len(m)):
        print(m[i])

def confusion_matrix(ac, pc): #arr1 is ac
    correct = 0
    incorrect = 0
    #[row[1] for row in A]
    matrix = [[0]*4 for i in range(4)]
    for i in range (0, -1 + min(len(ac.data), len(pc.data))): #maybe add 1?
        matrix[ac.data[i+1][1] + 1][pc.data[i][1] + 1] += 1 #the +1 is because of the bug for auto classifying, and also for shifting
        if (ac.data[i+1][1] == pc.data[i][1]):
            correct += 1
        else:
            incorrect += 1
    print("Accuracy : %.3f" % (correct / (correct + incorrect)))
    return matrix, correct, incorrect

def getCol(arr, col):
    return [row[col] for row in arr]

def skip_matrix(ac, pc):
    delay_mtx = delay_matrix(ac, pc)
    skip_mtx = [-1 * delay_mtx[0][0], -1 * delay_mtx[0][1], -1 * delay_mtx[0][2]]
    pc.resetDelayMtx()
    temp_skip = 0
    early_delay = [0, 0, 0]
    for i in range (0, -1 + min(len(ac.data), len(pc.data))):
        for j in range (3):
            duration_arr = ac.getDurationPeriod(j)
            if (i >= duration_arr[0] and i <= duration_arr[1]):
                if (i == duration_arr[0]):
                    pc.delay_mtx[0][j] -= early_delay[j]
                if (pc.status[j] == 0):
                    pc.status[j] == 1
                if (pc.getEvent(i) != j):
                    temp_skip += 1
                else:
                    skip_mtx[j] += temp_skip
                    temp_skip = 0
                if (i == duration_arr[1]):
                    pc.delay_mtx[1][j] -= temp_skip #action ended early, so negative delay
                    temp_skip = 0
            else:
                if (early_delay[j - 1] == 0 and early_delay[j - 2] == 0):
                    early_delay[j] += 1
                else:
                    early_delay = [0, 0, 0]
    pc.skip_mtx = skip_mtx
    pc.resetStatus()
    return skip_mtx


def delay_matrix(ac, pc): #note that you must run skip_matrix before delay_matrix
    delay_mtx = pc.delay_mtx
    ac_arr = ac.data
    pc_arr = pc.data
    for i in range (0, -1 + min(len(ac.data), len(pc.data))):
        for j in range (3):
            duration_arr = ac.getDurationPeriod(j)
            if (i >= duration_arr[0] and i <= duration_arr[1]): #within event period
                if (pc.getEvent(i) == j and pc.status[j] == 0):
                    pc.status[j] = 1
                    delay_mtx[0][j] = i - duration_arr[0]
            elif (pc.status[j] == 1 and i > duration_arr[1]):
                if (pc.getEvent(i) == j):
                    delay_mtx[1][j] += 1
                else:
                    pc.status[j] = 2
            #print("Duration :")
            #print(duration_arr)
    pc.delay_mtx = delay_mtx
    pc.resetStatus()
    return delay_mtx


main()
