# CompareClassification

CompareClassification is a small program for comparing the accuracy of video segmentations. The AC object originates from mechanically-labeled data and the PC object stems from auto-labeled data.

You will find:

  - CompareClassification.py (main)
  - Class files (AC and PC)
  - Sample labels (for format reference)

### Installation

CompareClassification requires at least Python 3.0 to run.

```sh
$ git clone https://gitlab.com/royceyang/CompareClassification.git
$ cd CompareClassification
```

To run:
```sh
$ python3 CompareClassification.py
```

If you want to output to a text file called out.txt:
```sh
$ python3 CompareClassification.py > out.txt
```

### Adding Your Own Data

  - Be sure to follow the same format as the given sample labels in AutoLabels.
  - By default, the program compares .txt's in MechanicalLabels with .txt's in AutoLabels/FuVersion. You may change this in CompareClassification.py.

### Versions

| Version Number | Features |
| ------ | ------ |
| 1.0.1 | Initial version: outputs confusion matrix and accuracy status. |
| 1.0.2 | Added feature: outputs average accuracy status across all tested labels. |
| 1.0.3 | Added feature: outputs skip_matrix, a matrix representing errors from jump discontinuity in classification. |
| 1.0.4 | Added feature: outputs delay_matrix, a matrix representing errors from delayed classification. |
| 1.0.5 | Added feature: outputs average jump errors and delay errors across all tested labels. |
| 1.0.6 | Improvement: improved delay_matrix logic. |
| 1.1.0 | Improvement: improved skip_matrix (for jump discontinuity errors) logic, now outputs average jump error, entry delay error, and exit delay error across all files. |
| 1.1.1 | Added feature: can now account for early delays, which is when an action is predicted before the actual action occurs. |
| 1.1.2 | Improvement: improved delay_matrix early exit error logic. |
| 1.1.3 | Improvement: added aggregate matrix data to output.|

### TODO

  - Fix formatting in sample labels (does not affect program execution).
