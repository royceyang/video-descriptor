"""
Class for auto labeled objects.
"""

import os

class PC: #PC stands for PredictedClassification

    counter = 0

    def __init__(self, filename):
        self.filename = filename
        self.status = [0, 0, 0]
        self.data = self.readToArray()
        self.delay_mtx = [[0,0,0],[0,0,0]]
        self.skip_mtx = [0, 0, 0]


    def resetStatus(self):
        self.status = [0, 0, 0]

    def resetDelayMtx(self):
        self.delay_mtx = [[0,0,0],[0,0,0]]

    def getFrameCount(self):
        return sum(1 for line in open(self.filename)) - 1 #-1 because of a bug in the labels

    def getVideoName(self):
        return os.path.splitext(os.path.basename(self.filename))[0]

    def getEvent(self, framenum):
        return self.data[framenum][1]

    def readToArray(self):
        readIn = []
        text_file = open(self.filename, "r")
        for line in text_file:
            if (len(line) > 1): #skip blank lines
                temp = line.strip('\n').split('\t')
                eventnum = -1
                if (temp[1] == "play with ball"):
                    eventnum = 0
                elif (temp[1] == "use bike"):
                    eventnum = 1
                elif (temp[1] == "sit on chair" or temp[1] == "person sit down"):
                    eventnum = 2
                readIn.append([int(temp[0]), eventnum])
        return readIn

    def getCol(self, arr, col):
        return [row[col] for row in arr]

    def getDurationPeriod(self, event):
        if (event == -1):
            print("Error : Cannot get duration of doing nothing!")
            return [-1, -1]
        else:
            temp_arr = self.data
            temp_col = self.getCol(temp_arr)
            return [temp_arr[temp_col.index(event)][0],
            temp_arr[len(temp_col) - 1 - temp_col[::-1].index(event)]]
