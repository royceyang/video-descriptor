"""
Class for mechnically labeled objects.
"""

import os

class AC: #AC stands for ActualClassification

    counter = 0

    def __init__(self, filename):
        self.filename = filename
        self.dur = self.initDuration()
        self.data = self.readToArray()

    def initDuration(self):
        return [[-1, -1],[-1, -1],[-1, -1]]

    """
    def getFrameCount(self):
        readIn = []
        text_file = open(self.filename, "r")
        for line in text_file:
            temp = line.split(' ').strip('\n')
            readIn.append(temp)
            ...
    """

    def getVideoName(self):
        return (os.path.splitext(os.path.basename(self.filename))[0])[:-6]

    def readToArray(self):
        readIn = []
        text_file = open(self.filename, "r")
        framecounter = 1
        for line in text_file:
            #print(framecounter)
            if (len(line) > 1): #skip blank lines
                temp = line.strip('\n').split(' ')
                #print(int(temp[1]))
                while (framecounter < int(temp[1])):
                    readIn.append([framecounter, -1])
                    framecounter += 1
                    #print("Nothing %d" % framecounter)
                self.dur[int(temp[0])][0] = framecounter - 1
                #print("Framecounter : %d, 1 : %d, 2 : %d" % (framecounter, int(temp[1]), int(temp[2])))
                while (framecounter >= int(temp[1]) and framecounter <= int(temp[2])):
                    readIn.append([framecounter, int(temp[0])])
                    framecounter += 1
                self.dur[int(temp[0])][1] = framecounter - 1
            else:
                framecounter += 1
        return readIn

    def setData(self, arr):
        self.data = arr

    """def getCol(self, arr, col):
        return [row[col] for row in arr]"""

    def getEvent(self, framenum):
        return self.data[framenum][1]

    def getDurationPeriod(self, event):
        return self.dur[event]

    """def getDurationPeriod(self, event):
        if (event == -1):
            print("Error : Cannot get duration of doing nothing!")
            return [-1, -1]
        else:
            try:
                temp_arr = self.data
                temp_col = self.getCol(temp_arr, event)
                return [temp_arr[temp_col.index(event)][0],
                temp_arr[len(temp_col) - 1 - temp_col[::-1].index(event)]]
            except ValueError:
                return [-1, -1]"""
